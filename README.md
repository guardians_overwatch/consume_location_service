### Prerequisites
Follow the instructions in the location-poc repository first.

#### Pull the docker image down (I'll eventually add how to create an image, but for now an image already exists)
```
$ docker pull danmikita/consumelocationservice:latest
```

---

### Kubernetes Related Things

#### Deploy and expose kubernetes pod
```
$ kubectl run consume-location-service --image=consumelocationservice:latest --port=8080

$ kubectl expose deployment consume-location-service --type=NodePort
```

# You can get an object via id like so
$ curl $(minikube service consume-location-service --url)/1
```


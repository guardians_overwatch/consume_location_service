package com.snyder616.locationpoc.model;

public class LocationGreeting {
  private final String greeting;


  public LocationGreeting(String greeting) {
    this.greeting = greeting;
  }

  public String getGreeting() {
    return greeting;
  }
}

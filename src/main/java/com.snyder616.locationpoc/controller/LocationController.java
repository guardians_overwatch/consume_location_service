package com.snyder616.locationpoc.controller;

import com.snyder616.locationpoc.model.LocationGreeting;
import com.snyder616.locationpoc.model.LocationPoc;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

@Controller public class LocationController {
    @RequestMapping(value = "/", method = RequestMethod.GET) public @ResponseBody
    LocationGreeting get() {

        RestAdapter retrofit = new RestAdapter.Builder()
            .setEndpoint("http://location-poc:8080")
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .build();

        GitHubService service = retrofit.create(GitHubService.class);

        LocationPoc response = service.listLocations(1);
        String name = response.getName();
        LocationGreeting greeting = new LocationGreeting("Hello, " + name);

        return greeting;

    }

    public interface GitHubService {
        @GET("/{id}")
        LocationPoc listLocations(@Path("id") int id);
    }
}
